/*
 * SOP_Centroid.h
 *
 *
 *    Copyright (c) 2010-2013 Sebastian H. Schmidt (sebastian<dot>h<dot>schmidt<at>googlemail<dot>com)
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.

 */

#include "SOP_Centroid.h"




//static PRM_Default    	useGroupsDefault = PRM_Default(0.0);
static PRM_Name			useGroupsName("enable_groups", "Use Groups");
static PRM_Default      useGroupsDefault[] = {
    PRM_Default( 3, "Primitives"),
    PRM_Default( 5, "Groups"),
    //PRM_Default( 3, "Attribute"),
};
static const char* 	useGroupsHelp = "Specify if you want to build Centroids from Primitives or Groups";

static PRM_Default    	keepGeometryDefault = PRM_Default(0.0);
static PRM_Name			keepGeometryName("keep", "Keep Geometry");
static const char*		keepGeometryHelp = "tick to keep the geometry when building the centroids";

static PRM_Default    	ignoreEmptyDefault = PRM_Default(1.0);
static PRM_Name			ignoreEmptyName("ignoreempty", "Ignore Empty Groups");
static const char* 		ignoreEmptyHelp = "ignore empty groups when generating Centroids from groups";

static PRM_Default multithreadingDefault(0);
static PRM_Name multithreadingName("threading", "Enable Threading");
static const char* multithreadingHelp = "use multithreading when generating centroids";


static PRM_Default		groupMethodDefault(0);
static PRM_Name		groupMethodName("technique", "Build Centroids based on ");
static PRM_Name groupMethodMenuNames[] =
{
    PRM_Name("bbox", "Bounding Box"),
    PRM_Name("points", "Points"),
    PRM_Name("bary", "Barycenter"),
    PRM_Name("obbox", "Oriented Bounding Box"),
    PRM_Name(0)
};
static PRM_ChoiceList groupMethodMenu((PRM_ChoiceListType)(PRM_CHOICELIST_EXCLUSIVE |PRM_CHOICELIST_REPLACE),groupMethodMenuNames);
static const char*  groupMethodHelp = "Specify which method to use when building centroids from groups,\n-using a axis-aligned bounding box arround the group, \n-the points of the group,  \n-the barycenter of the group, \n-or the oriented bounding box of the group" ;

static PRM_Default		primMethodDefault(0);
static PRM_Name		primMethodName("primtechnique", "Build Centroids based on ");
static PRM_Name primMethodMenuNames[] =
{
    PRM_Name("bbox", "Bounding Box"),
    PRM_Name("points", "Points"),
    PRM_Name("bary", "Barycenter"),
    PRM_Name(0)
};
static PRM_ChoiceList primMethodMenu((PRM_ChoiceListType)(PRM_CHOICELIST_EXCLUSIVE |PRM_CHOICELIST_REPLACE),primMethodMenuNames);
static const char*  primMethodHelp = "Specify which method to use when building centroids from primitives,\n-using a axis-aligned bounding box arround the group, \n-the points of the group,  \n-or the barycenter of the group" ;


/*
static PRM_Default		attribMethodDefault(0);
static PRM_Name		attribMethodName("attrtechnique", "Build Centroids based on ");
static PRM_Name attribMethodMenuNames[] =
{
    PRM_Name("bbox", "Bounding Box"),
    PRM_Name("points", "Points"),
    PRM_Name("bary", "Barycenter"),
    PRM_Name(0)
};
static PRM_ChoiceList attribMethodMenu((PRM_ChoiceListType)(PRM_CHOICELIST_EXCLUSIVE |PRM_CHOICELIST_REPLACE),attribMethodMenuNames);
*/


static PRM_Default	inheritPrimAttribTglDefault(0);
static PRM_Name		inheritPrimAttribTglName("inheritattrtgl", "Inherit Prim Attributes");
static const char*  inheritPrimAttribTglHelp = "toggle to inherit primitive attributes onto the centroids";

static PRM_Default	inheritPrimAttribStringDefault(0,"N");
static PRM_Name		inheritPrimAttribStringName("inheritattr", "Prim Attributes");
static const char*  inheritPrimAttribStringHelp = "specify attributes to inherit, * is supported";

static PRM_Name		inheritGrpNameName("inheritgroupname","Inherit Group Name");
static PRM_Default	inheritGrpNameDefault(0);
static const char*  inheritGrpNameHelp = "toggle if you want to inherit the group name onto the centroid";


/*
static PRM_Default	orientbboxTglDefault(0);
static PRM_Name		orientbboxTglName("obboxtgl", "Use Oriented BBox");
*/


static PRM_Default	bboxDimensionTglDefault(0);
static PRM_Name		bboxDimensionTglName("bboxdimtgl", "Add BBox Dimensions");
static const char*  bboxDimensionTglHelp = "toggle if you want to add bounding box size as a attribute to the centroid";


static PRM_Default	bboxOrientTglDefault(0);
static PRM_Name		bboxOrientTglName("bboxorienttgl", "Add BBox Orientation");
static const char*  bboxOrientTglHelp = "toggle if you want to add bounding box orientation as a attribute to the centroid";

/*
static PRM_Default	attribParmDefault(0,"id");
static PRM_Name		attribParmName("attribute", "Attribute");

static PRM_Default	attribTypeDefault(0);
static PRM_Name		attribTypeName("attribtype", "Attribute Type");
*/


PRM_Name sep1("seperator1","Seperator1");
PRM_Name sep2("seperator2","Seperator2");





PRM_Template
SOP_Centroid::myTemplateList[] = {


	PRM_Template(PRM_SWITCHER_EXCLUSIVE,  sizeof(useGroupsDefault)/sizeof(PRM_Default),
		                                &useGroupsName, useGroupsDefault,0,0,0,0,0,useGroupsHelp),
	// primititive
	PRM_Template(PRM_ORD		,1	,&primMethodName		,&primMethodDefault	,&primMethodMenu,0,0,0,0,primMethodHelp),

	PRM_Template(PRM_TOGGLE		,1	,&inheritPrimAttribTglName			,&inheritPrimAttribTglDefault,0,0,0,0,0,inheritPrimAttribTglHelp),
	PRM_Template(PRM_STRING		,1	,&inheritPrimAttribStringName		,&inheritPrimAttribStringDefault,0,0,0,0,0,inheritPrimAttribStringHelp),

	// groups
	PRM_Template(PRM_STRING		,1	,&PRMgroupName			,0	,&SOP_Node::groupMenu),
	
	
	PRM_Template(PRM_INT_J		,1	,&PRMentityName			,0	,&PRMentityMenuPointsAndPrimitives),
	
	//#if (HMAJOR==12) and (HMINOR>=1)	
	//PRM_Template(PRM_INT_J		,1	,&PRMentityName			,0	,&PRMentityMenu),//PRMentityMenu),
	//#endif

	PRM_Template(PRM_TOGGLE		,1	,&ignoreEmptyName		,&ignoreEmptyDefault,0,0,0,0,0,ignoreEmptyHelp),
	PRM_Template(PRM_ORD		,1	,&groupMethodName		,&groupMethodDefault	,&groupMethodMenu,0,0,0,0,groupMethodHelp),
	PRM_Template(PRM_TOGGLE		,1	,&inheritGrpNameName	,&inheritGrpNameDefault,0,0,0,0,0,inheritGrpNameHelp),


	PRM_Template(PRM_TOGGLE		,1  ,&bboxDimensionTglName		,&bboxDimensionTglDefault,0,0,0,0,0,bboxDimensionTglHelp),
	PRM_Template(PRM_TOGGLE		,1  ,&bboxOrientTglName			,&bboxOrientTglDefault,0,0,0,0,0,bboxOrientTglHelp),

	PRM_Template(PRM_SEPARATOR	,1	,&sep2),
	PRM_Template(PRM_TOGGLE		,1	,&keepGeometryName		,&keepGeometryDefault,0,0,0,0,0,keepGeometryHelp),
	PRM_Template(PRM_TOGGLE		,1	,&multithreadingName	,&multithreadingDefault,0,0,0,0,0,multithreadingHelp),

	PRM_Template(),
};

static UT_String orientAttrName("orient");
static UT_String sizeAttrName("size");
static UT_String grpAttrName("name");

void
newSopOperator(OP_OperatorTable *table)
{
     table->addOperator(new OP_Operator("Centroid",
                                        "Centroid",
                                        SOP_Centroid::myConstructor,
                                        SOP_Centroid::myTemplateList,
                                         1,
                                         1,
                                         0));
}



OP_Node *
SOP_Centroid::myConstructor(OP_Network *net, const char *name, OP_Operator *op)
{
    return new SOP_Centroid(net, name, op);
}

const char *
SOP_Centroid::inputLabel(unsigned idx ) const
{
	if (idx==0)
	{
		return "Geometry to generate centroids for";
	}
	else
	{
		return "Undefined Input ";
	}
}


SOP_Centroid::SOP_Centroid(OP_Network *net, const char *name, OP_Operator *op)   : SOP_Node(net, name, op)
{
	m_ignEmptyGroups 		= true;
	m_groupCMethod 		= CM_BBOX;
	m_primCMethod 		= CM_BBOX;
	m_attribCMethod 	= CM_BBOX;
	m_buildMode			= BM_PRIM;
	m_inheritGrpName			= 0;

	this->attrBBoxSizeDef 	= new UT_Vector3(1,1,1);
	this->attrBBoxOrientDef = new UT_Matrix3(1,0,0,0,1,0,0,0,1);
}

SOP_Centroid::~SOP_Centroid()
{

}

bool SOP_Centroid::getThreading(fpreal time)
{
	return evalInt(multithreadingName.getToken(),0,time);
}

SOP_Centroid::CentroidMode SOP_Centroid::getGroupMethod(fpreal time)
{
	int hlp = evalInt(groupMethodName.getToken(),0,time);
	CentroidMode ret_val = CM_BBOX;
	if ((hlp >=0) && (hlp < CM_OBBOX))
		ret_val = (CentroidMode)hlp;
	return ret_val;
}

SOP_Centroid::CentroidMode SOP_Centroid::getPrimMethod(fpreal time)
{
	int hlp =  evalInt(primMethodName.getToken(),0,time);
	CentroidMode ret_val = CM_BBOX;
	if ((hlp >=0) && (hlp < CM_OBBOX))
		ret_val = (CentroidMode)hlp;
	return ret_val;
}

/*
int SOP_Centroid::getAttribMethod(fpreal time)
{
	return evalInt(attribMethodName.getToken(),0,time);
}


UT_String SOP_Centroid::getAttribName(fpreal time)
{
	UT_String ret_val;
	evalString(ret_val,attribParmName.getToken(),0,time);
	return ret_val;
}

int SOP_Centroid::getAttribType(fpreal time)
{
	return evalInt(attribTypeName.getToken(),0,time);
}
*/

int SOP_Centroid::getUseGroups(fpreal time)
{
	return evalInt(useGroupsName.getToken(),0,time);
}

bool SOP_Centroid::inheritGrpNameVal(fpreal time)
{
	return evalInt(inheritGrpNameName.getToken(),0,time)==1;
}

int SOP_Centroid::getGroupType(fpreal time)
{
	return evalInt(PRMentityName.getToken(),0,time);
}

UT_String SOP_Centroid::getGroupString(fpreal time)
{
	UT_String ret_val;
	evalString(ret_val,PRMgroupName.getToken(),0,time);
	return ret_val;
}

bool SOP_Centroid::getKeepGeometry(fpreal time)
{
	return (evalInt(keepGeometryName.getToken(),0,time) > 0);

}

bool SOP_Centroid::getIgnoreEmptyGroups(fpreal time)
{
	return evalInt(ignoreEmptyName.getToken(),0,time);
}


bool SOP_Centroid::getInheritPrimAttribTgl(fpreal time)
{
	return (evalInt(inheritPrimAttribTglName.getToken(),0,time) > 0);
}

UT_String SOP_Centroid::getInheritPrimAttrib(fpreal time)
{
	UT_String ret_val;
	evalString(ret_val,inheritPrimAttribStringName.getToken(),0,time);
	return ret_val;
}
/*
bool SOP_Centroid::getUseOBBox(fpreal time)
{
	return (evalInt(orientbboxTglName.getToken(),0,time) > 0);
}*/

bool SOP_Centroid::getStoreBBoxDimension(fpreal time)
{
	return (evalInt(bboxDimensionTglName.getToken(),0,time) > 0);
}

bool SOP_Centroid::getStoreBBoxOrient(fpreal time)
{
	return (evalInt(bboxOrientTglName.getToken(),0,time) > 0);
}



unsigned SOP_Centroid::disableParms()
{
    unsigned changed = 0;

    //changed  += enableParm(PRMentityName.getToken(),this->getUseGroups());
    //changed  += enableParm(PRMgroupName.getToken(),this->getUseGroups());
    //changed  += enableParm(ignoreEmptyName.getToken(),this->getUseGroups());
    changed  += enableParm(inheritPrimAttribTglName.getToken(),  this->getUseGroups()!=1);
    changed  += enableParm(inheritPrimAttribStringName.getToken(), this->getInheritPrimAttribTgl() && this->getUseGroups()!=1);

    //changed  += enableParm(orientbboxTglName.getToken(), (this->getMethod()==0) || this->getMethod());

    return changed;
}


GU_Detail *SOP_Centroid::findCurrJobGdp(jobGDPMap *centroidGDPs ,const UT_JobInfo &info)
{
	GU_Detail *ret_val = NULL;
	jobGDPMap::iterator fGdp = centroidGDPs->find(info.job());
	if ( fGdp == centroidGDPs->end())
	{

		ret_val = new GU_Detail(m_baseGdp);
		{
			UT_AutoJobInfoLock a(info);
			(*centroidGDPs)[info.job()] = ret_val;
		}
	}
	else
	{
		ret_val = fGdp->second;
	}

	return ret_val;
}


// generate centroids based on primitive-groups
void SOP_Centroid::genCentroidsPrimGroupPartial( 	jobGDPMap *centroidGDPs
													//,jobIDMap* centroidBBoxAttribIDs
													,const UT_JobInfo &info)
{

	int i,n;



	GU_Detail *partCentroid = SOP_Centroid::findCurrJobGdp(centroidGDPs,info);

	GEO_AttributeHandle sizeGAH,orientGAH,grpNameGAH;
	if (m_addBBoxDimension)
	{
		sizeGAH = partCentroid->getPointAttribute(sizeAttrName);
	}
	if (m_addBBoxOrient)
	{
		 orientGAH = partCentroid->getPointAttribute(orientAttrName);
	}
	if (m_inheritGrpName)
	{
		grpNameGAH = partCentroid->getPointAttribute(grpAttrName);
	}

	int prGrpL = this->primGroupNames.entries();
	for (info.divideWork(prGrpL,i,n);i<n;i++)
	{
		UT_String primGrpName = UT_String(this->primGroupNames(i));
		GA_PrimitiveGroup *cPrimGrp = NULL;
		{
			GU_DetailHandleAutoWriteLock lock(this->myGdpHandle);
			cPrimGrp = (GA_PrimitiveGroup*) lock.getGdp()->primitiveGroups().find(primGrpName);
		}

		GU_Detail *tmpGdp = NULL;
		if ((m_ignEmptyGroups) || (cPrimGrp->entries() > 0))
		{
			UT_Vector3 center = UT_Vector3(0,0,0);
			UT_Matrix4 *oTransform = NULL;
			UT_Vector3 *oRadi = NULL;

			switch (this->m_groupCMethod)
			{
				case CM_POINTS:  // points (1)
				{
					GEO_Primitive *prim = NULL;
					UT_IntArray storedNum;
					storedNum.entries(0);

						for ( GA_GBPrimitiveIterator it(*this->gdp,cPrimGrp);(prim=GA_Detail::GB_MACRO_CAST(gdp, it.getPrimitive()));  ++it)
						{
							unsigned vCount = prim->getVertexCount();
							for (unsigned int j = 0; j< (vCount); j++)
							{
								GEO_Vertex vt = prim->getVertexElement(j);
								int ptnum = vt.getPt()->getNum();
								if (storedNum.find(ptnum)==(int)-1)
								{
									storedNum.append(ptnum);
									UT_Vector3 pos = UT_Vector3(vt.getPos());
									center+=pos;
								}
							}
						}
					center = center/(fpreal)storedNum.entries();
					break;

				}
				case CM_BARY: //bary center (2)
				{
					GEO_Primitive *prim = NULL;
					UT_Vector3Array centerList;
					UT_FloatArray areaList;
					center = UT_Vector3(0.0,0.0,0.0);
					fpreal totalArea = 0;
					unsigned ctr = 0;
					for ( GA_GBPrimitiveIterator it(*this->gdp,cPrimGrp);(prim=GA_Detail::GB_MACRO_CAST(gdp, it.getPrimitive()));  ++it)
					{
						ctr++;
						UT_Vector3 p = prim->baryCenter();
						fpreal a = prim->calcArea();
						centerList.append(p);
						areaList.append(a);
						totalArea+=a;

					}
					for (unsigned x = 0; x< centerList.entries();x++)
					{
						center+=(centerList[x] * (areaList[x]/totalArea));
					}
					break;
				}
				case CM_OBBOX: //4: // oriented bbox
				{

					UT_Matrix4 transform;
					UT_Vector3 radii;
					this->gdp->getOBBox(cPrimGrp,transform,radii);
					transform.getTranslates(center);
					oRadi = new UT_Vector3(radii);
					oTransform = new UT_Matrix4(transform);

					break;
				}
				default:
				case CM_BBOX: //0: // bbox
				{
					UT_BoundingBox bBox;
					this->gdp->getBBox(&bBox,cPrimGrp);
					center = bBox.center();
					break;
				}

			}
			//! we need bounding box dimension and or bbox orientation
			if ((m_addBBoxDimension) || (m_addBBoxOrient))
			{
				// we do not have a gdp, so we need a new
				if ((!oRadi) || (!oTransform))
				{
					oTransform  = new UT_Matrix4;
					oRadi = new UT_Vector3;

					if (tmpGdp) // we already have a seperated gdp with the group of interest
					{

						UT_Matrix4 hlpM;
						UT_Vector3 hlpV;
						tmpGdp->getOBBox(NULL,hlpM,hlpV);
						*oTransform = hlpM;
						*oRadi = hlpV;

					}
					else // we do not have a seperate gdp
					{

						UT_Matrix4 hlpM;
						UT_Vector3 hlpV;
						this->gdp->getOBBox(cPrimGrp,hlpM,hlpV);
						*oTransform = hlpM;
						*oRadi = hlpV;
					}
				}

			}
			{
				GEO_Point *newPt = NULL;

				newPt = partCentroid->appendPointElement();
				newPt->setPos(center);

				if ((m_addBBoxDimension) && (oRadi!=NULL) && (sizeGAH.isAttributeValid()))
				{
					sizeGAH.setElement(newPt);
					sizeGAH.setV3(*oRadi,0);
				}

				if ((m_addBBoxOrient) && (oTransform!=NULL) && (orientGAH.isAttributeValid()))
				{
					orientGAH.setElement(newPt);
					UT_Matrix3 hm = UT_Matrix3(*oTransform);
					orientGAH.setM3(hm,0);
				}
				if ((m_inheritGrpName) && (grpNameGAH.isAttributeValid()))
				{
					grpNameGAH.setElement(newPt);
					grpNameGAH.setString(primGrpName,0);
				}
			}
			// cleanup gdp data
			if (tmpGdp != NULL)
			{
				tmpGdp->clearAndDestroy();
				tmpGdp = NULL;
			}
			// cleanup radi data
			if (oRadi !=NULL)
			{
				delete oRadi;
				oRadi = NULL;
			}
			// cleanup transform data
			if (oTransform !=NULL)
			{
				delete oTransform;
				oTransform = NULL;
			}
		}

	}

}



void SOP_Centroid::genCentroidsPointGroupPartial(	jobGDPMap *centroidGDPs
													//,jobIDMap* centroidBBoxAttribIDs
													,const UT_JobInfo &info)
{
	int i,n;

	GU_Detail *partCentroid = SOP_Centroid::findCurrJobGdp(centroidGDPs,info);

	GEO_AttributeHandle sizeGAH,orientGAH,grpNameGAH;
	if (m_addBBoxDimension) // adding bbox-attribute
	{
		sizeGAH = partCentroid->getPointAttribute(sizeAttrName);
	}
	if (m_addBBoxOrient) // add bbox-orientation attribute
	{
		 orientGAH = partCentroid->getPointAttribute(orientAttrName);
	}
	if (m_inheritGrpName) // add group name attribute
	{
		grpNameGAH = partCentroid->getPointAttribute(grpAttrName);
	}

	int pgrl = this->pointGroupNames.entries();
	 // itterate over current groups
	for (info.divideWork(pgrl,i,n);i<n;i++)
	{
		// get group name
		UT_String pGrpName = UT_String(this->pointGroupNames(i));
		GA_PointGroup *cPointGrp = (GA_PointGroup*)this->gdp->pointGroups().find(pGrpName);


		// if we ignore or we have entries
		if ((m_ignEmptyGroups) || (cPointGrp->entries() > 0))
		{
			UT_Vector3 center(0,0,0);
			UT_BoundingBox *oBBox = NULL;
			UT_Matrix4 *oTrans = NULL;


			switch (this->m_groupCMethod)
			{
				case CM_BBOX:// centroids based on  BBox
				{
					UT_BoundingBox cBBox;

					((GEO_Detail*)this->gdp)->getPointBBox(&cBBox,cPointGrp);
					center = cBBox.center();
					break;
				}
				case CM_OBBOX: // centroids based on  oriented BBox
				{

					UT_BoundingBox cBBox;
					UT_Matrix4R cTrans;
					this->gdp->getPointBBox(cBBox,cTrans,cPointGrp);
					// store this for later use
					oBBox = new UT_BoundingBox(cBBox);
					oTrans = new UT_Matrix4(cTrans);

					center = cBBox.center();
					break;
				}
				case CM_POINTS:// centroids based on the average position of all the points
				default:
				{
					GEO_Point *ppt;
					unsigned ctr =0;
					GA_FOR_ALL_GROUP_POINTS (this->gdp,cPointGrp,ppt)
					{
						center+=UT_Vector3(ppt->getPos());
						ctr+=1;
					}
					center = center/(fpreal)ctr;
					break;
				}
			}
			// check if we're interested in dimensions or orientation, and check if we might already have the data 
			if (((m_addBBoxDimension) || (m_addBBoxOrient)) && ((oBBox == NULL) || (oTrans == NULL)))
			{
				if (oBBox == NULL)
				{
					oBBox = new UT_BoundingBox();
				}
				if (oTrans == NULL)
				{
					UT_Matrix4R cTrans; cTrans.identity();
					UT_BoundingBox mBox;
					this->gdp->getPointBBox(mBox,cTrans,cPointGrp);
					oTrans = new UT_Matrix4(cTrans);
					oBBox = new UT_BoundingBox(mBox);
				}
			}

			{
				// create the point
				GEO_Point *newPt;
				newPt = partCentroid->appendPointElement();
				// set the position 
				newPt->setPos(center);
				// set dimensions
				if ((m_addBBoxDimension) && (sizeGAH.isAttributeValid())  && (oBBox!=NULL))
				{
					sizeGAH.setElement(newPt);
					sizeGAH.setV3(oBBox->size(),0);
				}
				//set orientation
				if ((m_addBBoxOrient) && (orientGAH.isAttributeValid()) && (oTrans!=NULL))
				{
					orientGAH.setElement(newPt);
					orientGAH.setM3(UT_Matrix3(*oTrans),0);
				}
				//set group name
				if ((m_inheritGrpName) && (grpNameGAH.isAttributeValid()))
				{
					grpNameGAH.setElement(newPt);
					grpNameGAH.setString(pGrpName,0);
				}
			}
			//cleanup
			if (oTrans != NULL)
			{
				delete oTrans;
				oTrans = NULL;
			}
			if (oBBox != NULL)
			{
				delete oBBox;
				oBBox = NULL;
			}
		}
	}
}





//
//	Primitives Multithreaded
//  it also copies attributes from the primitive to the generated centroids
//

void SOP_Centroid::genCentroidsPrimitivesPartial(	jobGDPMap *centroidGDPs
													//,jobIDMap* centroidBBoxAttribIDs
													//,jobIDMap* centroidPrimAttribIDs
													,const UT_JobInfo &info)
{
	int i,n;

	// get gdp for the current jon
	GU_Detail *partCentroid = SOP_Centroid::findCurrJobGdp(centroidGDPs,info);

	// lets create a list of attributes to copy
	std::vector<attrCopy2> copyMyAttr;// = m_primAttrCopyList;

	// itterate over this list
	for (size_t i=0;i<m_primAttrCopyList.size();i++)
	{
		attrCopy2 tst;

		// set the primitive attribute handle
		tst.sgah = this->gdp->getPrimAttribute(m_primAttrCopyList[i].srcAttr.getAttribute()->getName());

		// set the point attribute hande
		tst.dgah = partCentroid->getPointAttribute(m_primAttrCopyList[i].destAttr.getAttribute()->getName());
		copyMyAttr.push_back(tst);
	}

	GEO_AttributeHandle sizeGAH,orientGAH;
	if (m_addBBoxDimension)
	{
		sizeGAH = partCentroid->getPointAttribute(sizeAttrName);
	}
	if (m_addBBoxOrient)
	{
		 orientGAH = partCentroid->getPointAttribute(orientAttrName);
	}


	//const GA_PrimitiveList &plist = this->gdp->getPrimitiveList();
	//for (info.divideWork(plist.entries(),i,n);i<n;i++)
	// lets go through all the primitives
	for (info.divideWork(this->gdp->primitives().entries(),i,n);i<n;i++)
	{

		//get the current one
		GEO_Primitive *currPrim = this->gdp->primitives().entry(i);
		UT_Vector3 center;
		UT_Matrix4 orient;


		orient.identity();

		// get the primitive method
		switch (this->m_primCMethod)
		{
			case CM_BBOX://0: //bbox
			{
				UT_BoundingBox cBBox;
				currPrim->getBBox(&cBBox);
				center = cBBox.center();
				break;
			}
			case CM_BARY:// barycentric
			{
				center = currPrim->baryCenter();
				break;
			}
			case CM_POINTS:// based on the points`
			default:
			{
				unsigned ct = currPrim->getVertexCount();
				center = UT_Vector3(0,0,0);
				for (unsigned x = 0; x<ct;x++)
				{
					center += (UT_Vector3(currPrim->getVertexElement(x).getPos()) * 1.0/ct);
				}
				break;
			}
		}

		// create the new points
		{
			GEO_Point *newPt;
			newPt = partCentroid->appendPointElement();
			newPt->setPos(center);

			if (m_addBBoxDimension)
			{
				UT_BoundingBox bBox;
				currPrim->getBBox(&bBox);
				sizeGAH.setElement(newPt);
				sizeGAH.setV3(bBox.size());

			}	


			// itterate over all attributes, tagged for copy
			for (size_t i=0;i<copyMyAttr.size();i++)
			{

				// set the provider and reveiver attribute handles
				copyMyAttr[i].sgah.setElement(currPrim);
				copyMyAttr[i].dgah.setElement(newPt);

				// if the attributes are not valid
				if (!((copyMyAttr[i].sgah.isAttributeValid()) && (copyMyAttr[i].dgah.isAttributeValid())))
				{
					// ignore this attribute
					continue;
				}

				// get the storrage clas
				GA_StorageClass sc = copyMyAttr[i].sgah.getAttribute()->getStorageClass();
				int fc = copyMyAttr[i].sgah.tupleSize();
				if (sc == GA_STORECLASS_FLOAT)
				{
					for (int j=0;j<fc;j++)
					{
						copyMyAttr[i].dgah.setF(copyMyAttr[i].sgah.getF(j),j);
					}
				}
				else if (sc == GA_STORECLASS_INT)
				{
					for (int j=0;j<fc;j++)
					{
						copyMyAttr[i].dgah.setI(copyMyAttr[i].sgah.getI(j),j);
					}
				}
				else if (sc == GA_STORECLASS_STRING)
				{
					for (int j=0;j<fc;j++)
					{
						UT_String cpString;
						copyMyAttr[i].sgah.getString(cpString,j);
						copyMyAttr[i].dgah.setString(cpString,j);
					} // for - tuple-size
				}// else if storage class String
			}//for attributes
		}// creating new poonts
	}//itteration over primitives
}

// create the generic geometry pointer, and adds the neccesary attributes
bool SOP_Centroid::createBaseGdp()
{



	m_baseGdp = new GU_Detail;
	m_baseGdpHdl.allocateAndSet(m_baseGdp,true);
	bool ret_val = true;

	if (m_addBBoxDimension)
	{

		//UT_Vector3 *sizeDef = new UT_Vector3(1,1,1);
		GU_DetailHandleAutoWriteLock wLock(m_baseGdpHdl);
		m_dimID = wLock.getGdp()->addFloatTuple(GA_ATTRIB_POINT,GA_SCOPE_PUBLIC,sizeAttrName.buffer(),3,GA_Defaults(1.0f));
	}
	if (m_addBBoxOrient)
	{
		//UT_Matrix4 *matDef = new UT_Matrix4();
		//matDef->identity();
		GU_DetailHandleAutoWriteLock wLock(m_baseGdpHdl);
		UT_Matrix3 def;
		def.identity();

		m_orientID = wLock.getGdp()->addFloatTuple(GA_ATTRIB_POINT,GA_SCOPE_PUBLIC,orientAttrName.buffer(),9,GA_Defaults(def.data(),9));
	}

	if (((m_buildMode == BM_PRIMGROUP) || (m_buildMode == BM_POINTGROUP)) && (m_inheritGrpName))
	{
		GU_DetailHandleAutoWriteLock wLock(m_baseGdpHdl);
		m_orientID = wLock.getGdp()->addStringTuple(GA_ATTRIB_POINT,GA_SCOPE_PUBLIC,grpAttrName.buffer(),1);
	}

	if ((this->inheritAttrib) && (m_buildMode==BM_PRIM))
	{
		// inheriting attributes
		for (int i=0;i<this->m_primAttrCopyList.size();i++)
		{
			attrCopy *cAttr =  &this->m_primAttrCopyList[i];

			GU_DetailHandleAutoWriteLock wLock(m_baseGdpHdl);
			GA_RWAttributeRef newAttr = wLock.getGdp()->addPointAttrib(cAttr->srcAttr.getAttribute());
			cAttr->destAttr = newAttr;


			if (cAttr->destAttr.isValid())
			{
				ret_val = ret_val && true;
			}
			else
			{
				ret_val = ret_val && false;
			}
		}
	}
	return ret_val;
}




OP_ERROR SOP_Centroid::cookMySop(OP_Context &context)
{
    OP_AutoLockInputs	autolock;

    if (autolock.lock(*this, context) >= UT_ERROR_ABORT) return error();


    UT_Interrupt*	 boss	 = UTgetInterrupt();

    this->duplicateSource(0,context);

    fpreal time 		= context.getTime();

    // do we ignore groups
    m_ignEmptyGroups 	= this->getIgnoreEmptyGroups(time);
    // wanna add bbox dimenstion attrib
    m_addBBoxDimension 	= this->getStoreBBoxDimension(time);
    // wanna add bbox orientation attrib
    m_addBBoxOrient		= this->getStoreBBoxOrient(time);
    // wanna inherit group names
    m_inheritGrpName		= this->inheritGrpNameVal(time);

    // init some vars
	m_dimID = GA_RWAttributeRef(0);
	m_orientID = GA_RWAttributeRef(0);
	m_baseGdp = NULL;
	

	int useGroups = this->getUseGroups();

	if (useGroups==0)
	{
		m_buildMode = BM_PRIM;
	}
	else if (useGroups == 1)
	{
		if (getGroupType()==0)
		{
			m_buildMode = BM_PRIMGROUP;
		}
		else
		{
			m_buildMode = BM_POINTGROUP;
		}

	}

	GU_Detail *centroidGDP = NULL;


	// preflight
	switch (m_buildMode)
	{
		case BM_PRIM: // based on primitives
		{

			this->inheritAttrib =this->getInheritPrimAttribTgl(time);
			this->attribNameList.clear();
			this->m_primAttrCopyList.clear();
			if (this->inheritAttrib)
			{
				UT_String primPattern = this->getInheritPrimAttrib(time);

				SOP_Centroid::matchPrimAttribList(this->gdp->primitiveAttribs(),primPattern,&this->m_primAttrCopyList);
			}


			break;
		}
		case BM_PRIMGROUP: // based on prim groups
		{
			this->m_groupCMethod = this->getGroupMethod(time);
			boss->opStart("Building Prim Groups");
			SOP_Centroid::matchGroupList(this->gdp->primitiveGroups(),this->getGroupString(), &this->primGroupNames);
			boss->opEnd();

			if (this->primGroupNames.entries()<1)
			{
				this->addWarning(SOP_ERR_BADGROUP ,"no primgroup found matching the given pattern");
			}
			break;
		}
		case BM_POINTGROUP: // based on prim groups
		{
			boss->opStart("Building Point Groups");
			SOP_Centroid::matchGroupList(this->gdp->pointGroups(),this->getGroupString(), &this->pointGroupNames);
			boss->opEnd();

			if (this->pointGroupNames.entries()<1)
			{
				this->addWarning(SOP_ERR_BAD_POINTGROUP ,"no pointgroup found matching the given pattern");
			}
			break;
		}
		default:
		{
			this->addMessage(SOP_ERR_BAD_POINTGROUP ,"Invalid Mode");
			break;
		}
	}


	this->createBaseGdp();

	if (error())
	{
	    if (!this->getKeepGeometry())
	    {
	    	this->gdp->clearAndDestroy();
	    }

	    this->unlockInputs();
		return error();
	}


	// now do something
	switch (m_buildMode)
	{
		case BM_PRIM: // based on primitives
		{

			jobGDPMap jobDetailMap;

			boss->opStart("Building Centroids");

			this->genCentroidsPrimitives(&jobDetailMap);
			centroidGDP = this->mergeJobGDPs(&jobDetailMap);
			
			boss->opEnd();
			break;
		}
		case BM_PRIMGROUP: // based on prim groups
		{
			jobGDPMap jobDetailMap;
			boss->opStart("Building Centroids");
			this->genCentroidsPrimGroup(&jobDetailMap);
			centroidGDP = this->mergeJobGDPs(&jobDetailMap);
			boss->opEnd();
			break;
		}
		case BM_POINTGROUP:
		{
			jobGDPMap jobDetailMap;
			boss->opStart("Building Centroids");
			this->genCentroidsPointGroup(&jobDetailMap);
			centroidGDP = this->mergeJobGDPs(&jobDetailMap);
			boss->opEnd();
			break;
		}
		default:
		{
			break;
		}
	}



	GU_Detail blank_gdp;
    if (!this->getKeepGeometry())
    {
    	this->gdp->copy(blank_gdp,GEO_COPY_START);
    }

    this->gdp->copy(*centroidGDP,GEO_COPY_ADD);
    this->gdp->copy(blank_gdp,GEO_COPY_END);


    m_baseGdpHdl.deleteGdp();
    this->unlockInputs();
	return error();
}


GU_Detail* SOP_Centroid::mergeJobGDPs(jobGDPMap *centroidGDPs)
{
	GU_Detail *ret_val = new GU_Detail();
	GU_Detail blank_gdp;
	int idx = 0;
    jobGDPMap::const_iterator jobIter;
    for (jobIter = centroidGDPs->begin(); jobIter != centroidGDPs->end(); ++jobIter)
    {
    	if (!idx)
    	{
    		ret_val->copy((*jobIter->second),GEO_COPY_START);
    	}
    	else
    	{
    		ret_val->copy((*jobIter->second),GEO_COPY_ADD);
    	}
    	idx++;
    }
	ret_val->copy(blank_gdp,GEO_COPY_END);
	return ret_val;
}

void SOP_Centroid::matchPrimAttribList(const GA_AttributeDict &attrList, UT_String patternString, std::vector<attrCopy> *primAttrsList)
{
	UT_WorkArgs 	patternArgs;
	patternString.tokenize(patternArgs," ");

	for (GA_AttributeDict::iterator it = attrList.begin();!it.atEnd();++it)
	{
		UT_String primAttrName = it.attrib()->getName();
		if (primAttrName.matchPattern(patternArgs))
		{
			attrCopy ne;
			ne.srcAttr = GA_ROAttributeRef(it.attrib());
			primAttrsList->push_back(ne);
		}

	}
}

//! @desc: finds attributes based on a given pattern-string and returns a list of attribute names that match the pattern
//! 
void SOP_Centroid::matchPrimAttribNameList(const GA_AttributeDict &attrList, UT_String patternString, UT_StringArray *primAttrsNameList)
{
	UT_WorkArgs 	patternArgs;
	patternString.tokenize(patternArgs," ");

	for (GA_AttributeDict::iterator it = attrList.begin();!it.atEnd();++it)
	{
		UT_String primName = it.attrib()->getName();
		if (primName.matchPattern(patternArgs))
		{
			primAttrsNameList->append(primName);
		}
	}
}


void SOP_Centroid::matchGroupList(GA_ElementGroupTable &groupList, UT_String patternString, UT_StringArray *pGroupNames)
{

	UT_String    	groupName;
	UT_WorkArgs 	patternArgs;
	patternString.tokenize(patternArgs," ");
	pGroupNames->clear();

	UT_PtrArray<  const GA_ElementGroup  * > grplist;
	groupList.getList(grplist);

	for (unsigned int i=0;i<grplist.entries();i++)
	{
		groupName.harden(grplist[i]->getName());
		if (groupName.matchPattern(patternArgs))
		{
			pGroupNames->append(groupName);
		}
	}
}



