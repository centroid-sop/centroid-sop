/*
 * SOP_Centroid.h
 *
 *
 *    Copyright (c) 2010-2013 Sebastian H. Schmidt   (sebastian<dot>h<dot>schmidt<at>googlemail<dot>com)
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.

 */

#ifndef SOP_CENTROID_H_
#define SOP_CENTROID_H_


#include <UT/UT_DSOVersion.h>


#include <UT/UT_ThreadedAlgorithm.h>
#include <UT/UT_WorkArgs.h>




#include <GA/GA_Detail.h>
#include <GA/GA_Attribute.h>
#include <GA/GA_AttributeDict.h>
#include <GA/GA_AttributeRef.h>
#include <GA/GA_ElementGroupTable.h>


#include <GU/GU_Detail.h>


#include <UT/UT_PtrArray.h>

#include <SOP/SOP_Node.h>

#include <PRM/PRM_Include.h>
#include <OP/OP_Operator.h>
#include <OP/OP_OperatorTable.h>
#include <OP/OP_AutoLockInputs.h>

#include <GU/GU_PrimGroup.h>
#include <GU/GU_PointGroup.h>

#include <UT/UT_Interrupt.h>



#include <map>
//#include "shsMacros.h"



class SOP_Centroid: public SOP_Node
{
	public:
		static OP_Node  *myConstructor(OP_Network*,
					 const char *,
					 OP_Operator *);
		static PRM_Template myTemplateList[];

	 protected:
							SOP_Centroid(OP_Network *net, const char *name, OP_Operator *op);
		virtual 			~SOP_Centroid();
		virtual OP_ERROR  	cookMySop(OP_Context &context);
		virtual unsigned	disableParms();
		//! @note: returns the label for the input
		//! @parm idx: input index
		virtual const char  *inputLabel(unsigned idx) const;

	 public:

			//! stores a sum of point locations and the number of added location to later calculate the overall centroid
			struct vectorHelper
			{
				UT_Vector3 adder;
				uint count;
			};
			//! stores the barycenter of a primitive and its area to later calculate its contribution to the overall centroid
			struct baryHelper
			{
				UT_Vector3 bary;
				fpreal area;
			};
			//! a list of barycenters and there area
			typedef std::vector<baryHelper> baryList;

			struct baryObject
			{
				baryList allBary;
				fpreal allArea;
			};


			enum CentroidMode {CM_BBOX = 0,
					 CM_POINTS,
					 CM_BARY,
					 CM_OBBOX};

			enum BuildMode {BM_PRIM = 0,
					 BM_PRIMGROUP,
					 BM_POINTGROUP};

	private:

			//! a map where for each possible integer attribute we have a barycenter list
			typedef std::map<int,baryObject> intAttrBaryMap;

			//! defines a map where a integer attribute maps to a VectorCentroid
			typedef std::map<int,vectorHelper> intAttrVectorMap;


			//! a map where for each possible string attribute we have a barycenter list
			typedef std::map<UT_String,baryObject> stringAttrBaryMap;

			//! defines a map where a string maps to a vectorCentroid
			typedef std::map<UT_String,vectorHelper> stringAttrVectorMap;



			//! defines a map where a integer attribute maps to a Bounding Box
			typedef	std::map<int,UT_BoundingBox> intAttrBBoxMap;

			//! defines a map where a string maps to a boundingBox
			typedef std::map<UT_String,UT_BoundingBox> stringAttrBBoxMap;



			//! a job map of of a intAttrVectorMap so each thread/job got its own
			typedef std::map<int,intAttrVectorMap> jobIntVectorMap;

			//! a job map of of a intAttrBBoxMap so each thread/job got its own
			typedef std::map<int,intAttrBBoxMap >	jobIntBBoxMap;

			//! a job map of of a stringAttrVectorMap so each thread/job got its own
			typedef std::map<int,stringAttrVectorMap> jobStringVectorMap;

			//! a job map of of a stringAttrBBoxMap so each thread/job got its own
			typedef std::map<int,stringAttrBBoxMap> jobStringBBoxMap;


			//! a job map of of a stringAttrBaryMap so each thread/job got its own
			typedef	std::map<int,stringAttrBaryMap> jobStringBaryMap;

			//! a job map of of a intAttrBaryMap so each thread/job got its own
			typedef	std::map<int,intAttrBaryMap> jobIntBaryMap;



			//! note: a map of gdps so every thread has its own gdp to add centroids poins to
			typedef std::map<int,GU_Detail*> jobGDPMap;



			//! note: a map of id's so every thread has its own ids to e.g. attributes
			//typedef std::map<int,UT_IntArray*> jobIDMap;



	 private:

			//! @note: sets up the theaded methods to generate centroids for primitives
			THREADED_METHOD1( SOP_Centroid
								,(gdp->primitives().entries() > 100) && getThreading()
								,genCentroidsPrimitives
								,jobGDPMap*, centroidGDPs
								//,jobIDMap*, centroidBBoxAttribIDs
								//,jobIDMap*, centroidPrimAttribIDs
			);

			//! @note: generates the centroids for primitives
			void genCentroidsPrimitivesPartial(
												jobGDPMap *centroidGDPs
												//,jobIDMap* centroidBBoxAttribIDs
												//,jobIDMap* centroidPrimAttribIDs
												, const UT_JobInfo &info);

			//! @note: sets up the theaded methods to generate centroids for point groups
			THREADED_METHOD1( SOP_Centroid,
								(pointGroupNames.entries() > 20) && getThreading()
								,genCentroidsPointGroup
								,jobGDPMap*, centroidGDPs);
								//,jobIDMap*, centroidBBoxAttribIDs


			//! @note: generates the centroids for point groups
			void genCentroidsPointGroupPartial(jobGDPMap *centroidGDPs
											//,jobIDMap* centroidBBoxAttribIDs
											,const UT_JobInfo &info);


			//! @note: sets up the theaded methods to generate centroids prim groups
			THREADED_METHOD1( SOP_Centroid
								,(primGroupNames.entries() > 20) && getThreading()
								,genCentroidsPrimGroup
								,jobGDPMap*, centroidGDPs);
								//,jobIDMap*, centroidBBoxAttribIDs




			//! @note: generates the centroids for primitive groups
			void genCentroidsPrimGroupPartial(jobGDPMap *centroidGDPs
												//,jobIDMap* centroidBBoxAttribIDs
												,const UT_JobInfo &info);





			//! merges given centroid job gdps to one and returns them
			GU_Detail *mergeJobGDPs(jobGDPMap *centroidGDPs);

	 private:

			//! @note: generates a list of group names based on the given group list specified through the pattern string
			//! @param[in] groupList A list of existing group names
			//! @param[in] patternString The pattern to select a bunch of groups-names from the goup-list
			//! @param[out] pGroupNames A pointer to a list where the found group-names get added to
			static void matchGroupList(GA_ElementGroupTable &groupList, UT_String patternString, UT_StringArray *pGroupNames);


			struct attrCopy
			{
				GA_ROAttributeRef srcAttr;
				GA_RWAttributeRef destAttr;
			};

			struct attrCopy2
			{
				GEO_AttributeHandle sgah;
				GEO_AttributeHandle dgah;

			};

			//! @note: matches given attribute dictionary for attributes matching the given pattern
			//! @returns: a list of attributes that match that pattern
			static void matchPrimAttribList(const GA_AttributeDict &attrList, UT_String patternString, std::vector<attrCopy> *primAttrs);

			//! @note: matches given attribute dictionary for attributes matching the given pattern
			//! @param[in] attrList A dictionary of existing attributes
			//! @param[in] patternString The pattern which is used to find the given attributes based on there names
			//! @param[out] primAttrsNameList  A list of attributes -names that match that pattern
			static void matchPrimAttribNameList(const GA_AttributeDict &attrList, UT_String patternString, UT_StringArray *primAttrsNameList);



			//!  @note: looks through given gdpMap to find the if there are already gdp entries for the current job, or creates a new
			//!  @return: the found/created gdp
			//!  @param[in] centroidGDPs 			The gdbmap which contains a list of gdps for each job
			//!  @param[in] info 					The job information
			
			
			GU_Detail *findCurrJobGdp(jobGDPMap *centroidGDPs ,const UT_JobInfo &info);

			//! @note: creates the basic gdp where each thread makes its own copy from
			bool createBaseGdp();

	

			//! @note: a function that returns the ui status of a toggle which method should be used when generating the centroids
			//! @param time: the time when the ui-parameter should be evaluated
			bool		getThreading(fpreal time =0.0);


			//!  function that returns the ui status of a toggle which method should be used when generating the centroids for groups
			//! @param time: the time when the ui-parameter should be evaluated
			CentroidMode 		getGroupMethod(fpreal time = 0.0);

			//!  function that returns the ui status of a toggle which method should be used when generating the centroids for primitives
			CentroidMode 		getPrimMethod(fpreal time = 0.0);

			//!  function that returns the ui status of a toggle which method should be used when generating the centroids for points/primitives that share the same attribute
			int 		getAttribMethod(fpreal time = 0.0);

			//! @note: a function that returns the ui status of a toggle if groups should be used to generate the centroids
			//! @param time: the time when the ui-parameter should be evaluated
			int 		getUseGroups(fpreal time = 0.0);
			//! @note: a function that returns the ui status of a menue what type of groups should be used
			//! @param time: the time when the ui-parameter should be evaluated
			int 		getGroupType(fpreal time = 0.0);
			//! @note: a function that returns the ui status of a string which contains names of groups that should be processed
			//! @param time: the time when the ui-parameter should be evaluated
			UT_String 	getGroupString(fpreal time = 0.0);

			//! @note: a function that returns the ui status of a toggle parameter if the incoming geometry should be kept
			//! @return: if the incoming geometry should be kept or not
			//! @param time: the time when the ui-parameter should be evaluated
			bool 		getKeepGeometry(fpreal time = 0.0);


			int getAttribType(fpreal time = 0.0);

			UT_String getAttribName(fpreal time = 0.0);

			//! @note: a function that returns the ui status of a toggle parameter if the incoming geometry should be kept
			//! @return: if the incoming geometry should be kept or not
			//! @param time: the time when the ui-parameter should be evaluated
			bool getIgnoreEmptyGroups(fpreal time = 0.0);

			//! @note: a function that returns if the ui-toggle for inheriting primitive attributes to the centroids is ticked
			//! @param time: the time when the ui-parameter should be evaluated
			bool getInheritPrimAttribTgl(fpreal time = 0.0);

			//! @note: a function that returns the list of primitive attributes that should be inherited/transfered to the centroids
			//! @param time: the time when the ui-parameter should be evaluated
			UT_String getInheritPrimAttrib(fpreal = 0.0);

			//! @note: a function that returns if the oriented bbox should be used either for the method and/or the bbox dimensions/orientation
			//! @param time: the time when the ui-parameter should be evaluated
			bool getUseOBBox(fpreal time = 0.0);

			//! @note: a function that returns if the BBox Dimension have to be stored with the centroid
			//! @param time: the time when the ui-parameter should be evaluated
			bool getStoreBBoxDimension(fpreal time = 0.0);

			//! @note: a function that returns if the BBox orientation have to be stored with the centroid
			//! @param: time: the time when the ui-parameter should be evaluated
			bool getStoreBBoxOrient(fpreal time = 0.0);

			//! @node: a function that returns if the group name should be stored on the centroid
			//! @param: time: the time when the ui-parameter should be evaluated
			bool inheritGrpNameVal(fpreal time = 0.0);

		UT_StringArray primGroupNames;
		UT_StringArray pointGroupNames;

		bool m_ignEmptyGroups;
		CentroidMode m_groupCMethod;
		CentroidMode m_primCMethod;
		CentroidMode m_attribCMethod;

		bool m_inheritGrpName;

		//! @note: if centroids should be build from primitives pt/prim groups
		BuildMode m_buildMode;

		//! @note: if a oriented bbox should be used
		bool useOBBox;

		//! @note: if attributes should be inherited
		bool inheritAttrib;
		//! @note: a list of attributes that should be inherited
		UT_StringArray attribNameList;



		// list containing information for copying attributes
		std::vector<attrCopy> m_primAttrCopyList;

		//! @note: if the bbox orientation should be added to the centroid
		bool m_addBBoxOrient;

		//! @note: if the bbox dimension should be added to the centroid
		bool m_addBBoxDimension;


		 UT_Vector3 *attrBBoxSizeDef;
		 UT_Matrix3 *attrBBoxOrientDef;

		 GA_Attribute *attrBBoxSize;
		 GA_Attribute *attrBBoxOrient;

		 //! @note: id of the prim attribute which holds the dimension of the bbox
		GA_RWAttributeRef m_dimID;
		//! @note: id of the prim attribute which holds the orientation of the bbox
		GA_RWAttributeRef m_orientID;


		
		//! @note: base gdp created as blueprint copied for each job
		GU_Detail *m_baseGdp;
		//! @note: the handle for the baseGdp;
		GU_DetailHandle m_baseGdpHdl;





		//! @note: id-list of the primitive attributes which need to be copied from the primitive
		
};





#endif /* SOP_Centroid_H_ */
